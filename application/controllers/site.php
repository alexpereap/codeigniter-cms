<?php

class Site extends CI_Controller{

     function __construct(){
        parent::__construct();

        if( !$this->session->userdata('admin_data') &&  $this->router->method != 'login' && $this->router->method != 'check_login_credentials' ){

            redirect( site_url('site/login') );
        }

        $this->load->library('grocery_CRUD');
    }

    public function index(){

        // test commit
        $this->load->view("cms/opener");
        $this->load->view("cms/closure");
    }

    public function login(){

        if( $this->session->userdata('admin_data') ){
            redirect( site_url() );
        }

        $this->load->view('cms/login');
    }

    public function check_login_credentials(){

        $result = $this->main->logInCmsUser( $_POST );
        echo json_encode( array('result' => $result) );
    }
}