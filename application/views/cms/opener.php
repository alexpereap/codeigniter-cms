<!DOCTYPE html>
<html lang="en" ng-app="cmsApp" >
  <head>
    <base href="<?php echo base_url(); ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex,nofollow">

    <title></title>


    <!-- Custom styles for this template -->
    <style>
    body {
      padding-top: 20px;
      padding-bottom: 20px;
    }

    .navbar {
      margin-bottom: 20px;
    }

  .ui-autocomplete-loading {
        background: white url("images/ui-anim_basic_16x16.gif") right center no-repeat;
    }

    </style>



    <?php
      if( isset($css_files) )
      foreach($css_files as $file): ?>
      <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>

    <?php
      if( isset($js_files) )
      foreach($js_files as $file): ?>
      <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>


    <?php if( !isset( $js_files ) ): ?> <script src="<?php echo base_url('js/jquery-1.11.3.min.js') ?>" ></script> <?php endif; ?>


    <!-- Bootstrap core CSS -->
    <link href="vendor/tw_bootstrap/css/bootstrap.css" rel="stylesheet">

     <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="vendor/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="container">
    <input type="hidden" id="baseUrl" value="<?php echo base_url();?>"  >

      <!-- Static navbar -->
      <div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>cms">Cms</a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="<?php if( isset($in_food_list) ): ?>active<?php endif ?>" ><a href="<?php echo site_url('food_list') ?>">Lista de almuerzos</a></li>
              <li class="<?php if( isset($in_promos) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/promos">Programar almuerzos</a></li>
              <!-- Tvs -->


            <li class="dropdown <?php if( isset($in_codes) ): ?>active<?php endif ?>"  >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li class="<?php if( isset($in_codes_list) ): ?>active<?php endif ?>" ><a href="#">item 1</a></li>
                  <li class="<?php if( isset($in_codes_text) ): ?>active<?php endif ?>" ><a href="#">item 2</a></li>
                </ul>
              </li>

            </ul>

           <ul class="nav navbar-nav navbar-right">
                <li><a title="cerrar sesión" href="<?php echo base_url() ?>cms/logout">Cerrar sesión <span class="glyphicon glyphicon-off" ></span></a></li>
            </ul>

          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </div>


